<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $count = User::all()->count();
        $faker = Factory::create('pt_BR');
        if (0 == $count) {
            for ($i = 0; $i < 10; $i++) {
                DB::table('users')->insert([
                    'name' => $faker->name,
                    'email' => $faker->email,
                    'password' => $faker->password
                ]);
            }
            $user = User::create([
                'name' => 'Renato',
                'email' => 'cpdrenato@gmail.com',
                'email_verified_at' => null,
                'password' => bcrypt('password'),
            ]);
        } else {
            echo 'Qtde: ' . $count . ' Records Inside Database!';
        }
    }
}
