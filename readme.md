<p align="center"><img src="2022-03-30_19-27.png" width="600"></p>

# Laravel Dockerize Project - Redis | Postgresql | Elasticsearch | RabbitMQ

## Build

```sh
docker-compose up --build -d 
```
## Start / Stop

```sh
docker-compose up -d & docker-compose down
```

## Permission folder

```sh
docker-compose exec -d app chmod -R 777 storage
```

## Migrate

```sh
docker-compose exec -d app php artisan migrate --seed
```

## Queue for rabbitMQ

```sh
docker-compose exec -d app php artisan queue:work
```


`./vendor/bin/php-cs-fixer fix`

- http://localhost:8000/
- http://localhost:15672/#/

- https://faker.readthedocs.io/en/master/locales/pt_BR.html

## Renato Lucena - 2022